package asgn2GUI;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * Creates a JPanel in which graphical components are laid out to represent the cargo manifest.
 *
 * @author Rohan Treadwell n9184856
 * @version 1.0
 */
public class CargoCanvas extends JPanel {

    private static final int WIDTH = 120;
    private static final int HEIGHT = 50;
    private static final int HSPACE = 10;
    private static final int VSPACE = 20;

    private final CargoManifest cargo;

    private ContainerCode toFind;

    /**
     * Constructor
     *
     * @param cargo The <code>CargoManifest</code> on which the graphics is based so that the
     * number of stacks and height can be adhered to.
     */
    public CargoCanvas(CargoManifest cargo) {
        this.cargo = cargo;
        setName("Canvas");
    }

    /**
     * Highlights a container.
     *
     * @param code ContainerCode to highlight.
     */
    public void setToFind(ContainerCode code) {
    	toFind = code;
    	this.repaint();       
    	//implementation here - don't forget to repaint
    }

    /**
     * Draws the containers in the cargo manifest on the Graphics context of the Canvas.
     *
     * @param g The Graphics context to draw on.
     */
    @Override
    public void paint(Graphics g) {
		super.paintComponent(g); 

    	int y = VSPACE;
		int stack = 0;
		while (true){
	    	int x = HSPACE;
	    	try{
				FreightContainer[] containerstack = cargo.toArray(new Integer(stack));
				g.setColor(Color.BLACK);
				g.drawLine(HSPACE/2, y, HSPACE/2, y + HEIGHT);
				for (int container=0; container< containerstack.length; container++){
					drawContainer(g, containerstack[container], x, y);
					x = x + WIDTH + HSPACE;
			 	}			 
			}catch (ManifestException e){
				return;
			}
			y = y + VSPACE + HEIGHT;
			stack++;
		}
    }

    /**
     * Draws a container at the given location.
     *
     * @param g The Graphics context to draw on.
     * @param container The container to draw - the type determines the colour and ContainerCode is
     *            used to identify the drawn Rectangle.
     * @param x The x location for the Rectangle.
     * @param y The y location for the Rectangle.
     */
    private void drawContainer(Graphics g, FreightContainer container, int x, int y) {
    	//Implementation here 
    	//Feel free to use some other method structure here, but this is the basis for the demo. 
    	//Obviously you need the graphics context and container as parameters. 
    	//But you can also use images if you wish. 
     	
		if ((toFind != null) && (toFind.equals(container.getCode()))) {
			g.setColor(Color.YELLOW);
		}else if (container.getClass().toString().contains("DangerousGoodsContainer")) {
			g.setColor(Color.RED);  
		}else if (container.getClass().toString().contains("RefrigeratedContainer")) {
			g.setColor(Color.BLUE);
		}else if (container.getClass().toString().contains("GeneralGoodsContainer")) {
			g.setColor(Color.GREEN);
		}
		g.drawRect(x, y, WIDTH, HEIGHT);
		g.fillRect(x, y, WIDTH, HEIGHT);
		
		g.setColor(Color.WHITE);
		g.drawString(container.getCode().toString(), x+20,y+30);
	}
}
