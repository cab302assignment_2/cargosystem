package asgn2GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import asgn2Codes.ContainerCode;
import asgn2Containers.FreightContainer;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * The main window for the Cargo Manifest graphics application.
 *
 * @author Rohan Treadwell n9184856
 * @version 1.0
 */
public class CargoFrame extends JFrame {

    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    private JButton btnLoad;
    private JButton btnUnload;
    private JButton btnFind;
    private JButton btnNewManifest;

    private CargoCanvas canvas;

    private JPanel pnlControls;
    private JPanel pnlDisplay;

    private CargoManifest cargo;

    /**
     * Constructs the GUI.
     *
     * @param title The frame title to use.
     * @throws HeadlessException from JFrame.
     */
    public CargoFrame(String title) throws HeadlessException {
        super(title);

        constructorHelper();
        disableButtons();
        redraw();
        setVisible(true);
    }

    /**
     * Initialises the container display area.
     *
     * @param cargo The <code>CargoManifest</code> instance containing necessary state for display.
     */
    private void setCanvas(CargoManifest cargo) {
        if (canvas != null) {
            pnlDisplay.remove(canvas);
        }
        if (cargo == null) {
            disableButtons();
        } else {
            canvas = new CargoCanvas(cargo);
        	canvas.setSize(WIDTH, HEIGHT);
        	getContentPane().add(canvas,BorderLayout.CENTER);
        	pnlDisplay.setLayout(new BorderLayout());
            pnlDisplay.add(canvas);
            enableButtons();  
        }
        redraw();
    }

    /**
     * Enables buttons for user interaction.
     */
    @SuppressWarnings("deprecation")
	private void enableButtons() {
    	btnUnload.setEnabled(true);
    	btnLoad.setEnabled(true);
    	btnFind.setEnabled(true);
     }

    /**
     * Disables buttons from user interaction.
     */
    private void disableButtons() {   
    	btnUnload.setEnabled(false); 
    	btnLoad.setEnabled(false); 
    	btnFind.setEnabled(false); 
    }

    /**
     * Initialises and lays out GUI components.
     */
    private void constructorHelper() {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        btnLoad = createButton("Load", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.resetCanvas();
                        CargoFrame.this.doLoad();
                    }
                };
                SwingUtilities.invokeLater(doRun);
            }
        });
        btnUnload = createButton("Unload", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
                Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.resetCanvas();
                        CargoFrame.this.doUnload();
                    }
                };
                SwingUtilities.invokeLater(doRun);			
			}
        });
        btnFind = createButton("Find", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
                Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.resetCanvas();
                        CargoFrame.this.doFind();
                    }
                };
                SwingUtilities.invokeLater(doRun);
			}
        });
        btnNewManifest = createButton("New Manifest", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.resetCanvas();
                        CargoFrame.this.setNewManifest();
                    }
                };
                SwingUtilities.invokeLater(doRun);
			}
        });
        
        pnlDisplay= new JPanel();        
        this.getContentPane().add(pnlDisplay);  
        pnlControls=  createControlPanel(); 
        this.getContentPane().add(pnlControls, BorderLayout.PAGE_END);  
        
        repaint();
    }

    /**
     * Creates a JPanel containing user controls (buttons).
     *
     * @return User control panel.
     */
    private JPanel createControlPanel() {
    	JPanel pnlBtn = new JPanel(); 
    	GridBagConstraints constraint = new GridBagConstraints(); 
     	addToPanel(pnlBtn, btnNewManifest, constraint , 30, 5 , 40, 20);
    	addToPanel(pnlBtn, btnLoad, constraint , 30, 5 , 40, 20);
     	addToPanel(pnlBtn, btnUnload, constraint , 30, 5 , 40, 20);
    	addToPanel(pnlBtn, btnFind, constraint , 30, 5 , 40, 20);	
    	return pnlBtn;
    	
    }    
    
    protected void addToPanel(JPanel panel, Component component, GridBagConstraints constraints,
            int x, int y, int width, int height) {
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = width;
        constraints.gridheight = height;
        panel.add(component, constraints);
    }

    /**
     * Factory method to create a JButton and add its ActionListener.
     *
     * @param name The text to display and use as the component's name.
     * @param btnListener The ActionListener to add.
     * @return A named JButton with ActionListener added.
     */
    private JButton createButton(String name, ActionListener btnListener) {
        JButton btn = new JButton(name);
        btn.setName(name);
        btn.addActionListener(btnListener);
        return btn;
    }

    /**
     * Initiate the New Manifest dialog which sets the instance of CargoManifest to work with.
     */
    private void setNewManifest() {
    	cargo = ManifestDialog.showDialog(this);  
    	setCanvas(cargo);
    	resetCanvas();

    }

    /**
     * Turns off container highlighting when an action other than Find is initiated.
     */
    private void resetCanvas() {
    	if (canvas != null) canvas.setToFind(null);  
    }

    /**
     * Initiates the Load Container dialog.
     */
    private void doLoad() {
        FreightContainer freightContainer = LoadContainerDialog.showDialog(this);   
        if (freightContainer != null) {
	        try {
				cargo.loadContainer(freightContainer);
			} catch (ManifestException e) {
				JOptionPane.showMessageDialog(null,e.getMessage() , "Load Container Error", JOptionPane.ERROR_MESSAGE);
			}
        }
        canvas.setToFind(null);
        redraw();   
    }

    private void doUnload() {
        ContainerCode containerId = ContainerCodeDialog.showDialog(this);  
        if (containerId != null) {
	        try {
				cargo.unloadContainer(containerId);
			} catch (ManifestException e) {
				JOptionPane.showMessageDialog(null,e.getMessage() , "Unload Container Error", JOptionPane.ERROR_MESSAGE);
			}
        }
        canvas.setToFind(null);   
        redraw(); 
    }

    private void doFind() {
    	ContainerCode containerId = ContainerCodeDialog.showDialog(this);  
        if (cargo.whichStack(containerId) == null){
        	if(containerId != null){
        		JOptionPane.showMessageDialog(null,"Sorry,The Container '" +containerId+ "'  is not in the Cargo.", "Find Error", JOptionPane.ERROR_MESSAGE);
        	}
        }else{ 
        	canvas.setToFind(containerId); 
        	redraw();   
        }  
    }

    private void redraw() {
        invalidate();
        validate();
        repaint();
    }
}
