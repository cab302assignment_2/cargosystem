package asgn2GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import asgn2Exceptions.CargoException;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * Creates a dialog box allowing the user to enter parameters for a new <code>CargoManifest</code>.
 *
 * @author Rohan Treadwell n9184856
 * @version 1.0
 */
public class ManifestDialog extends AbstractDialog {

    private static final int HEIGHT = 160;
    private static final int WIDTH = 250;

    private JTextField txtNumStacks;
    private JTextField txtMaxHeight;
    private JTextField txtMaxWeight;

    private CargoManifest manifest;

    /**
     * Constructs a modal dialog box that gathers information required for creating a cargo
     * manifest.
     *
     * @param parent the frame which created this dialog box.
     */
    private ManifestDialog(JFrame parent) {
        super(parent, "Create Manifest", WIDTH, HEIGHT);
       // System.out.println("Setting name");
        setName("New Manifest");
        setResizable(false);
    }

    /**
     * @see AbstractDialog.createContentPanel()
     */
    @Override
    protected JPanel createContentPanel() {

        txtNumStacks = createTextField(8, "Number of Stacks");
        txtMaxHeight = createTextField(8, "Maximum Height");
        txtMaxWeight = createTextField(8, "Maximum Weight");
        
        GridBagConstraints constraint = new GridBagConstraints(); 
        JPanel toReturn = new JPanel();
        toReturn.setLayout(new GridBagLayout());
        
 
        addToPanel(toReturn, new JLabel("Number of Stacks:"), constraint, 30, 30, 40, 20);
        addToPanel(toReturn, txtNumStacks, constraint , 100, 30 , 40, 20);
        addToPanel(toReturn, new JLabel("Maximum Height:"), constraint, 30, 60, 40, 20);
        addToPanel(toReturn, txtMaxHeight, constraint , 100, 60 , 40, 20);
        addToPanel(toReturn, new JLabel("Maximum Weight:"), constraint, 30, 90, 40, 20);
        addToPanel(toReturn, txtMaxWeight, constraint , 100, 90 , 40, 20);
  
        return toReturn;
    }

    /*
     * Factory method to create a named JTextField
     */
    private JTextField createTextField(int numColumns, String name) {
        JTextField text = new JTextField();
        text.setColumns(numColumns);
        text.setName(name);
        return text;
    }

    @Override
    protected boolean dialogDone(){
    	Integer num;
    	Integer height;
    	Integer weight;
    	try{
    		num  = new Integer(txtNumStacks.getText().trim());
    	} catch(NumberFormatException e) {
    		JOptionPane.showMessageDialog(null, "Number of stacks is not an Integer.", "Manifest Error", JOptionPane.ERROR_MESSAGE);
      		return false;
    	}
 
    	try{
    		height = new Integer(txtMaxHeight.getText().trim());
    	} catch(NumberFormatException e) {
    		JOptionPane.showMessageDialog(null, "Maximum height is not an Integer.", "Manifest Error", JOptionPane.ERROR_MESSAGE);
			return false;
    	}
    	try{
    		weight = new Integer(txtMaxWeight.getText().trim());
    	} catch(NumberFormatException e) {
    		JOptionPane.showMessageDialog(null, "Maximum Weight is not an Integer.", "Manifest Error", JOptionPane.ERROR_MESSAGE);
    		return false;
    	}
    	try{
  		   	manifest =  new CargoManifest(num, height, weight);
    	} catch (ManifestException e) {
    		JOptionPane.showMessageDialog(null, "CargoException: "+e.getMessage(), "Manifest Error!", JOptionPane.ERROR_MESSAGE);
    		return false;
		}
	   	return true;
    }

    /**
     * Shows the <code>ManifestDialog</code> for user interaction.
     *
     * @param parent - The parent <code>JFrame</code> which created this dialog box.
     * @return a <code>CargoManifest</code> instance with valid values.
     */
    public static CargoManifest showDialog(JFrame parent) {
    	ManifestDialog dialog = new ManifestDialog(parent); 
//    	dialog.dialogDone();
    	dialog.setVisible(true);
    	return dialog.manifest;
     }
}