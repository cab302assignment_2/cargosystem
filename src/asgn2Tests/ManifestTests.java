package asgn2Tests;

/* Some valid container codes used in the tests below:
 * INKU2633836
 * KOCU8090115
 * MSCU6639871
 * CSQU3054389
 * QUTU7200318
 * IBMU4882351
 */




import static org.junit.Assert.*;

import org.junit.*;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.InvalidContainerException;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * Test for <code>CargoManifest</code> class.
 * 
 * @author Rohan Treadwell n9184856
 * @version 1.0
 **/
public class ManifestTests {
	//Implementation Here	
	
	private CargoManifest CM;
	private FreightContainer FC;
	
	private ContainerCode CC;
	private ContainerCode CC1;
	private ContainerCode CC2;
	private ContainerCode CC3;
	private ContainerCode CC4;
	private ContainerCode CC5;
	private ContainerCode CC6;
	private ContainerCode CC7;
	private ContainerCode CC8;
	private ContainerCode CC9;
	
	private DangerousGoodsContainer Danger;
	private DangerousGoodsContainer Danger1;
	private DangerousGoodsContainer Danger2;
	private DangerousGoodsContainer Danger3;
	
	private RefrigeratedContainer RC;
	private RefrigeratedContainer RC1;
	private RefrigeratedContainer RC2;
	
	private GeneralGoodsContainer GGC;
	private GeneralGoodsContainer GGC1;
	private GeneralGoodsContainer GGC2;

	private int zeroStack = 0;
	private int zeroHeight = 0;
	private int zeroWeight = 0;
	private int stack = 1;
	private int stack2 = 2;
	private int stack3 = 3;
	private int stack5 = 5;
	private int InvalidStack = -1;
	private int height = 1;
	private int height2 = 2;
	private int height3 = 3;
	private int height5 = 5;
	private int InvalidHeight = -5;
	private int weight4 = 4;
	private int weight5 = 5;
	private int weight9 = 9;
	private int weight30 = 30;
	private int weight90 = 90;
	private int InvalidWeight = -5;
	
	@Before
	public void setUp() throws Exception{
		CC = new ContainerCode("MSCU6639871");
		CC1 = new ContainerCode("ASHU3506054");
		CC2 = new ContainerCode("FNPU6952577");
		CC3 = new ContainerCode("YGVU2940286");
		CC4 = new ContainerCode("IEWU6647210");
		CC5 = new ContainerCode("VBUU0333023");
		CC6 = new ContainerCode("CQNU9467827");
		CC7 = new ContainerCode("XMCU9157391");
		CC8 = new ContainerCode("ZGGU7132842");
	}
	
	/**
	 * Test case for <code>CargoManifest</code> class.
	 * The CargoManifest function need to insert the value of MaxStacks, maxHeight and maxWeight
	 *
	 *
	 *
	 * @throws ManifestException
	 */
	@Test
	public void Teststacks() throws ManifestException{
		CM = new CargoManifest(stack, height,weight5);
	}
	
	/**
	 * Test case for <code>CargoManifest</code> class.
	 * The CargoManifest function need to insert the value of MaxStacks, maxHeight and maxWeight
	 * If the value inserted as 0 it will still pass.
	 * 
	 * 
	 * 
	 * @throws ManifestException
	 */
	@Test
	public void ZeroValues() throws ManifestException{
		CM = new CargoManifest(zeroStack,zeroHeight,zeroWeight);
	}
	
	/**
	 * Test case for the <code>CargoManifest</code> class.
	 * The CargoManifest function need to insert the value of MaxStacks, maxHeight and maxWeight
	 * 
	 * 
	 * 
	 * @throws ManifestException if the MaxStack values inserted is negative value
	 */
	@Test (expected = ManifestException.class)
	public void NegativeStacks() throws ManifestException{
		CM = new CargoManifest(InvalidStack,height5,weight30);
	}
	
	/**
	 * Test case for the <code>CargoManifest</code> class.
	 * The CargoManifest function need to insert the value of MaxStacks, maxHeight and maxWeight
	 * 
	 * 
	 * 
	 * @throws ManifestException if the maxHeight values inserted is negative value
	 */
	@Test (expected = ManifestException.class)
	public void NegativeHeight() throws ManifestException{
		CM = new CargoManifest(stack, InvalidHeight,weight5);
	}
	
	/**
	 * Test case for the <code>CargoManifest</code> class.
	 * The CargoManifest function need to insert the value of MaxStacks, maxHeight and maxWeight
	 * 
	 * 
	 * 
	 * @throws ManifestException if the maxWeight values inserted is negative value
	 */
	@Test (expected = ManifestException.class)
	public void NegativeWeight() throws ManifestException{
		CM = new CargoManifest(stack, height,InvalidWeight);
	}
	
	/**
	 * Test case for the <code>CargoManifest</code> class.
	 * The CargoManifest function need to insert the value of MaxStacks, maxHeight and maxWeight
	 * 
	 * 
	 * 
	 * @throws ManifestException if the maxWeight values inserted is negative value
	 */
	@Test (expected = ManifestException.class)
	public void AllNegative() throws ManifestException{
		CM = new CargoManifest(InvalidStack, InvalidHeight,InvalidWeight);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class. 
	 * To load container, it requires the container must not be <code>null</code><br> <br>
	 * 
	 * By default the container is null if the container code is not assigned
	 * 
	 * 
	 * 
	 * @throws ManifestException if the container loaded is <code>null</code>
	 * */
	@Test (expected = ManifestException.class) 
	public void loadNullContainer() throws ManifestException{
		CM = new CargoManifest(stack, height,weight5);
		CM.loadContainer(FC);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * The container must be assign with a valid <code>ContainerCode</code>
	 * 
	 * The container will be load if the container informations are correct.
	 * 
	 * 
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void loadOneContainer() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack, height,weight5);
		Danger = new DangerousGoodsContainer(CC,weight4,9);
		CM.loadContainer(Danger);
		
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * If the <code>grossWeight</code> of container > maxWeight of <code>CargoManifest</code> ,
	 * 
	 * @throws ManifestException If the container is over weight
	 * @throws InvalidContainerException
	 */
	@Test (expected = ManifestException.class) 
	public void loadContainerMoreThanWeightLimit() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack, height,weight5);
		Danger = new DangerousGoodsContainer(CC,weight9,9);
		CM.loadContainer(Danger);
	}	
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * If the total sum of <code> grossWeight</code>  of all the containers > maxWeight of <code>CargoManifest</code> ,<br> <br>
	 * 
	 * CargoManifest maxWeight = 7<br>
	 * DangerousGoodsContainer, grossWeight = 4 <br>
	 * RefrigeratedContainer, grossWeight = 4
	 * If the total sum of containers, 4+4 is more than the maxWeight of cargo, 7.
	 * 
	 * 
	 * @throws ManifestException if the Total Weight of containers is > <code>maxWeight</code> limit
	 * @throws InvalidContainerException
	 */
	@Test (expected = ManifestException.class) 
	public void loadContainersTotalMoreThanWeightLimit() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack3, height5,weight5);
		Danger = new DangerousGoodsContainer(CC,4,4);
		RC = new RefrigeratedContainer(CC2,4,1);
		CM.loadContainer(Danger);
		CM.loadContainer(RC);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * If the total sum of <code>grossWeight</code> of all the containers > maxWeight of <code> CargoManifest</code> ,
	 *
	 *
	 * @throws ManifestException if the container stack is more than the <code>maxStack</code>.
	 * @throws InvalidContainerException
	 */
	@Test  
	public void loadTwoContainers() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack3,height5,weight9);
		Danger = new DangerousGoodsContainer(CC,4,4);
		RC = new RefrigeratedContainer(CC2,4,1);
		CM.loadContainer(Danger);
		CM.loadContainer(RC);
	}

	/**
	 * Test case for <code>loadContainer</code> class.
	 * If the total stack of container loaded is > the maxStack of Cargo
	 * 
	 * 
	 * @throws ManifestException if the number of container loaded is > <code>maxStack</code>
	 * @throws InvalidContainerException
	 */
	@Test (expected = ManifestException.class) 
	public void loadContainerMoreThanMaxStack() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack2,height,weight9);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		RC = new RefrigeratedContainer(CC2,4,1);
		GGC = new GeneralGoodsContainer(CC3,5);
		CM.loadContainer(Danger);
		CM.loadContainer(RC);
		CM.loadContainer(GGC);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * 
	 * 
	 * @throws ManifestException if the code of a container having a same code to another container which have been loaded.
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class) 
	public void loadContainersWithSameCode() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack2,height,weight9);
		Danger = new DangerousGoodsContainer(CC,4,1);
		RC = new RefrigeratedContainer(CC,4,1);
		CM.loadContainer(Danger);
		CM.loadContainer(RC);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * If the type of containers are same then it will be in the same stack, so it will be counted into the maxHeight.
	 * 
	 * 
	 * @throws ManifestException if the height of container exceeding the <code>maxHeight</code>
	 * @throws InvalidContainerException
	 */
	@Test //(expected = ManifestException.class) 
	public void loadSameHeight() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * If the type of containers are same then it will be in the same stack, so it will be counted into the maxHeight.
	 * 
	 * @throws ManifestException if the height of container exceeding the <code>maxHeight</code>
	 * @throws InvalidContainerException
	 * */
	@Test (expected = ManifestException.class) 
	public void loadExtraHeight() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		Danger3 = new DangerousGoodsContainer(CC4,4,1);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		CM.loadContainer(Danger3);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * If the type of containers are same then it will be in the same stack, so it will be counted into the maxHeight.
	 * 
	 * @throws ManifestException if the weight of container exceeding the <code>maxHeight</code>
	 * @throws InvalidContainerException
	 * */
	@Test //(expected = ManifestException.class) 
	public void load3ContainerPerStack() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack3,height3,weight90);
		
		Danger = new DangerousGoodsContainer(CC,4,1);
		Danger1 = new DangerousGoodsContainer(CC1,4,1);
		Danger2 = new DangerousGoodsContainer(CC2,4,1);
		
		RC = new RefrigeratedContainer(CC3,4,1);
		RC1 = new RefrigeratedContainer(CC4,4,1);
		RC2 = new RefrigeratedContainer(CC5,4,1);
		
		GGC = new GeneralGoodsContainer(CC6,5);
		GGC1 = new GeneralGoodsContainer(CC7,5);
		GGC2 = new GeneralGoodsContainer(CC8,5);
		
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		
		CM.loadContainer(RC);
		CM.loadContainer(RC1);
		CM.loadContainer(RC2);
		
		CM.loadContainer(GGC);
		CM.loadContainer(GGC1);
		CM.loadContainer(GGC2);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * If the type of containers are same then it will be in the same stack, so it will be counted into the maxHeight.
	 * 
	 * @throws ManifestException if the height of container exceeding the <code>maxHeight</code> of cargo
	 * @throws InvalidContainerException
	 * */
	@Test (expected = ManifestException.class) 
	public void load3ContainerPerStackOverHeight() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack3,height2,weight90);
		Danger = new DangerousGoodsContainer(CC,4,1);
		Danger1 = new DangerousGoodsContainer(CC1,4,2);
		
		RC = new RefrigeratedContainer(CC3,4,1);
		RC1 = new RefrigeratedContainer(CC4,4,1);
		RC2 = new RefrigeratedContainer(CC5,4,1);
		
		GGC = new GeneralGoodsContainer(CC6,5);
		GGC1 = new GeneralGoodsContainer(CC7,5);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		
		CM.loadContainer(RC);
		CM.loadContainer(RC1);
		CM.loadContainer(RC2);
		
		CM.loadContainer(GGC);
		CM.loadContainer(GGC1);
	}
	
	/**
	 * Test case for <code>loadContainer</code> class.
	 * The maxWeight of the cargo is based on the total sum of containers loaded into the cargo.
	 * 
	 * 
	 * @throws ManifestException if the weight of container exceeding the <code>maxWeight</code>
	 * @throws InvalidContainerException
	 * */
	@Test (expected = ManifestException.class) 
	public void load3ContainerPerStackOverWeight() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack3,height3,weight30);
		Danger = new DangerousGoodsContainer(CC,4,1);
		Danger1 = new DangerousGoodsContainer(CC1,4,2);
		Danger2 = new DangerousGoodsContainer(CC2,4,9);
		
		RC = new RefrigeratedContainer(CC3,4,1);
		RC1 = new RefrigeratedContainer(CC4,4,1);
		RC2 = new RefrigeratedContainer(CC5,4,1);
		
		GGC = new GeneralGoodsContainer(CC6,5);
		GGC1 = new GeneralGoodsContainer(CC7,5);
		GGC2 = new GeneralGoodsContainer(CC8,5);
		
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		
		CM.loadContainer(RC);
		CM.loadContainer(RC1);
		CM.loadContainer(RC2);
		
		CM.loadContainer(GGC);
		CM.loadContainer(GGC1);
		CM.loadContainer(GGC2);
	}
		
	/**
	 * Test for <code>unloadContainer</code> class.
	 * if the code have loaded into the cargo then it will be unloaded successfully.
	 * 
	 * 
	 * @throws ManifestException if the Container code is not in the cargo
	 * @throws InvalidContainerException
	 */
	@Test
	public void unloadContainer() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack5,height,weight9);
		GGC = new GeneralGoodsContainer(CC3,5);
		CM.loadContainer(GGC);
		CM.unloadContainer(CC3);
	}
	
	/**
	 * Test for <code>unloadContainer</code> class.
	 * If the code have loaded into the cargo then it will be unloaded successfully.
	 * 
	 * 
	 * @throws ManifestException if the Container code is not in the cargo
	 * @throws InvalidContainerException
	 */
	@Test (expected = ManifestException.class) 
	public void unloadSameContainerTwice() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack5,height,weight30);
		GGC = new GeneralGoodsContainer(CC3,5);
		CM.loadContainer(GGC);
		CM.unloadContainer(CC3);
		CM.unloadContainer(CC3);
	}
	
	/**
	 * Test for <code>unloadContainer</code> class.
	 * if the code have loaded into the cargo then it will be unloaded successfully.
	 * 
	 * 
	 * @throws ManifestException if the Container code is not in the cargo
	 * @throws InvalidContainerException
	 */
	@Test (expected = ManifestException.class) 
	public void unloadContainerNoBoard() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack5,height,weight30);
		GGC = new GeneralGoodsContainer(CC3,5);
		CM.loadContainer(GGC);
		CM.unloadContainer(CC);
	}
	
	/**
	 * Test for <code>unloadContainer</code> class.
	 * if the code have loaded into the cargo then it will be unloaded successfully.
	 * 
	 * 
	 * @throws ManifestException if the Container code is <code>null</code>
	 * @throws InvalidContainerException
	 */
	@Test (expected = ManifestException.class) 
	public void unloadNullContainer() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack5,height,weight30);
		CM.unloadContainer(CC9);
	}
	
	/**
	 * Test for <code>unloadContainer</code> class.
	 * If the code have loaded into the cargo then it will be unloaded successfully.
	 * If there are any container is stack above the container which need to be unload 
	 * 
	 * @throws ManifestException if there is another container above the <code>Container Code</code> which need to be unloaded.
	 * @throws InvalidContainerException
	 */
	@Test (expected = ManifestException.class) 
	public void unloadUnaccessible() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		CM.unloadContainer(CC1);
	}
	
	/**
	 * Test for <code>unloadContainer</code> class.
	 * If the code have loaded into the cargo then it will be unloaded successfully.
	 * If there are any container is stack above the container which need to be unload 
	 * 
	 * @throws ManifestException if there is another container above the <code>Container Code</code> which need to be unloaded.
	 * @throws InvalidContainerException
	 */
	@Test
	public void unloadFromTop() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		
		CM.unloadContainer(CC3);
		CM.unloadContainer(CC2);
		CM.unloadContainer(CC1);
	}
	
	/**
	 * Test for <code>unloadContainer</code> class.
	 * If the code have loaded into the cargo then it will be unloaded successfully.
	 * If there are any container is stack above the container which need to be unload, 
	 * meanwhile the container will be unloaded if it is on the top of stack.
	 * 
	 * @throws ManifestException if there is another container above the <code>Container Code</code> which need to be unloaded.
	 * @throws InvalidContainerException
	 */
	@Test //(expected = ManifestException.class) 
	public void unloadContainerOnTop() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		CM.unloadContainer(CC3);
	}
	
	/**
	 * Test for <code>unloadContainer</code> class.
	 * If the code have loaded into the cargo then it will be unloaded successfully.
	 * If there are any container is stack above the container which need to be unload, 
	 * meanwhile the container will be unloaded if it is on the top of stack.
	 * 
	 * @throws ManifestException if there is another container above the <code>ContainerCode</code> which need to be unloaded.
	 * @throws InvalidContainerException
	 */
	@Test (expected = ManifestException.class) 
	public void unloadContainerFromBottom() throws ManifestException, InvalidContainerException{
		CM = new CargoManifest(stack,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		CM.unloadContainer(CC1);
		CM.unloadContainer(CC2);
		CM.unloadContainer(CC3);
	}
	
	/**
	 * Test for <code>whichStack</code> class.
	 * If the container code is loaded into the cargo then it will be able to get the stack location of the container.
	 * Else it will return <code>null</code>
	 * If the container type is different, then it will not in the same stack 
	 * The stack will be different even though the type of containers are the same if it have exceed the maxHeight
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void CheckWhichStack() throws ManifestException, InvalidContainerException{		
		CM = new CargoManifest(stack5,height5,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		RC = new RefrigeratedContainer(CC2,4,1);
		GGC = new GeneralGoodsContainer(CC3,5);
		CM.loadContainer(Danger);
		CM.loadContainer(RC);
		CM.loadContainer(GGC);
		
		Integer i = 0;
		Integer ii = 1;
		Integer iii = 2;
		assertEquals(i, CM.whichStack(CC1));
		assertEquals(ii, CM.whichStack(CC2));
		assertEquals(iii, CM.whichStack(CC3));
	}
	
	/**
	 * Test for <code>whichStack</code> class.
	 * If the container code is loaded into the cargo then it will be able to get the stack location of the container.
	 * Else it will return <code>null</code>
	 * If the container type is different, then it will not in the same stack
	 * The stack will be different even though the type of containers are the same if it have exceed the maxHeight
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void CheckNullwhichStack() throws ManifestException, InvalidContainerException{		
		CM = new CargoManifest(stack5,height5,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		RC = new RefrigeratedContainer(CC2,4,1);
		GGC = new GeneralGoodsContainer(CC3,5);
		CM.loadContainer(Danger);
		CM.loadContainer(RC);
		CM.loadContainer(GGC);
		
		assertEquals(null, CM.whichStack(CC4));
	}
	
	/**
	 * Test for <code>whichStack</code> class.
	 * If the container code is loaded into the cargo then it will be able to get the stack location of the container.
	 * Else it will return <code>null</code>
	 * If the container type is different, then it will not in the same stack 
	 * The stack will be different even though the type of containers are the same if it have exceed the maxHeight
	 * 
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void CheckSameStack() throws ManifestException, InvalidContainerException{		
		CM = new CargoManifest(stack5,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		
		Integer i = 0;
		assertEquals(i, CM.whichStack(CC1));
		assertEquals(i, CM.whichStack(CC2));
		assertEquals(i, CM.whichStack(CC3));
	}
	
	/**
	 * Test for <code>whichStack</code> class.
	 * If the container code is loaded into the cargo then it will be able to get the stack location of the container.
	 * Else it will return <code>null</code>
	 * If the container type is different, then it will not in the same stack 
	 * The stack will be different even though the type of containers are the same if it have exceed the maxHeight
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void CheckSameTypeDiffStack() throws ManifestException, InvalidContainerException{		
		CM = new CargoManifest(stack5,height2,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		
		Integer i = 0;
		Integer ii = 1;
		assertEquals(i, CM.whichStack(CC1));
		assertEquals(i, CM.whichStack(CC2));
		assertEquals(ii, CM.whichStack(CC3));
	}
	
	/**
	 * Test for <code>howHigh</code> class.
	 * If the container code is loaded into the cargo then it will be able to get the height location of the container.
	 * Else it will return <code>null</code>
	 * If the container type is different, then it will not in the same stack 
	 * The stack will be different even though the type of containers are the same if it have exceed the maxHeight
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void CheckSameStackWhichHeight() throws ManifestException, InvalidContainerException{		
		CM = new CargoManifest(stack5,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		
		Integer i = 0;
		Integer ii = 1;
		Integer iii = 2;
		assertEquals(i, CM.howHigh(CC1));
		assertEquals(ii, CM.howHigh(CC2));
		assertEquals(iii, CM.howHigh(CC3));
	}
	
	/**
	 * Test for <code>howHigh</code> class.
	 * If the container code is loaded into the cargo then it will be able to get the height location of the container.
	 * Else it will return <code>null</code>
	 * If the container type is different, then it will not in the same stack 
	 * The stack will be different even though the type of containers are the same if it have exceed the maxHeight
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void CheckDiffStackWhichHeight() throws ManifestException, InvalidContainerException{		
		CM = new CargoManifest(stack5,height,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		
		Integer i = 0;
		assertEquals(i, CM.howHigh(CC1));
		assertEquals(i, CM.howHigh(CC2));
		assertEquals(i, CM.howHigh(CC3));
	}
	
	/**
	 * Test for <code>howHigh</code> class.
	 * If the container code is loaded into the cargo then it will be able to get the height location of the container.
	 * Else it will return <code>null</code>
	 * If the container type is different, then it will not in the same stack 
	 * The stack will be different even though the type of containers are the same if it have exceed the maxHeight
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void CheckDiffTypeWhichHeight() throws ManifestException, InvalidContainerException{		
		CM = new CargoManifest(stack5,height3,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		RC = new RefrigeratedContainer(CC2,4,1);
		GGC = new GeneralGoodsContainer(CC3,5);
		CM.loadContainer(Danger);
		CM.loadContainer(RC);
		CM.loadContainer(GGC);
		
		Integer i = 0;
		assertEquals(i, CM.howHigh(CC1));
		assertEquals(i, CM.howHigh(CC2));
		assertEquals(i, CM.howHigh(CC3));
	}

	/**
	 * Test for <code>howHigh</code> class.
	 * If the container code is loaded into the cargo then it will be able to get the height location of the container.
	 * Else it will return <code>null</code>
	 * If the container type is different, then it will not in the same stack 
	 * The stack will be different even though the type of containers are the same if it have exceed the maxHeight
	 * 
	 * @throws ManifestException
	 * @throws InvalidContainerException
	 */
	@Test
	public void CheckWhichHeightNull() throws ManifestException, InvalidContainerException{		
		CM = new CargoManifest(stack5,height,weight30);
		Danger = new DangerousGoodsContainer(CC1,4,1);
		Danger1 = new DangerousGoodsContainer(CC2,4,1);
		Danger2 = new DangerousGoodsContainer(CC3,4,2);
		CM.loadContainer(Danger);
		CM.loadContainer(Danger1);
		CM.loadContainer(Danger2);
		
		assertEquals(null, CM.howHigh(CC5));
	}
	
}