package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.regex.Pattern;

import javax.swing.JFrame;

import org.fest.swing.edt.FailOnThreadViolationRepaintManager;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asgn2GUI.CargoFrame;
import asgn2GUI.CargoTextFrame;

/**
 * Defines FEST Swing tests for use cases of the Cargo Manifest system.
 *
 * @author Yen Wei Chai n9388621
 * @version 1.2
 */
public class CargoFrameTests {

    private static final String MAXIMUM_WEIGHT = "Maximum Weight";
    private static final String MAXIMUM_HEIGHT = "Maximum Height";
    private static final String NUMBER_OF_STACKS = "Number of Stacks";
    private static final String START_BARS = "|| ";
    private static final String BETWEEN_BARS = " || ";
    private static final String END_BARS = " ||";
    private static final String START_FOUND_BARS = "||*";
    private static final String END_FOUND_BARS = "*||";
    private static final String START_FOUND_BETWEEN_BARS = " ||*";
    private static final String END_FOUND_BETWEEN_BARS = "*|| ";
    private static final String NEW_LINE = "\n";
    private static final String TEMPERATURE2 = "Temperature";
    private static final String GOODS_CATEGORY = "Goods Category";
    private static final String CONTAINER_WEIGHT = "Container Weight";
    private static final String CONTAINER_CODE = "Container Code";
    private static final String CONTAINER_INFORMATION = "Container Information";
    private static final String CARGO_TEXT_AREA = "Cargo Text Area";
    private static final String FIND = "Find";
    private static final String NEW_MANIFEST = "New Manifest";
    private static final String UNLOAD = "Unload";
    private static final String LOAD = "Load";
    private static final String CONTAINER_TYPE = "Container Type";
    private static final String GENERAL_GOODS = "General Goods";
    private static final String REFRIGERATED_GOODS = "Refrigerated Goods";
    private static final String DANGEROUS_GOODS = "Dangerous Goods";
    private static final String CATEGORY_2 = "2";
    private static final String DANGER_HIGH_CATEGORY = "12";
    private static final String TEMPERATURE_MINUS_4 = "-4";
    private static final String TEMPERATURE_HIGH_100 = "100";
    
    private static final String CODE_1 = "ABCU1234564";
    private static final String CODE_2 = "ZZZU6549874";
    private static final String CODE_3 = "JHGU1716760";
    private static final String CODE_4 = "MSCU6639871";
    private static final String CODE_5 = "ASHU3506054";
    private static final String CODE_6 = "FNPU6952577";
    private static final String CODE_7 = "YGVU2940286";
    private static final String CODE_8 = "IEWU6647210";
    private static final String CODE_9 = "XMCU9157391";
    
    private static final String INVALID_CODE_LENGTH = "XMCU915739";
    private static final String INVALID_OWNER_CODE_INTEGER = "X1CU9157391";
    private static final String INVALID_OWNER_CODE_LOWER_CASE = "ygvU2940286";
    private static final String INVALID_IDENTIFIER_CODE_INTEGER = "JHG21716760";
    private static final String INVALID_IDENTIFIER_CODE_LOWER_CASE = "JHGu1716760";
    private static final String INVALID_SERIAL_NUMBER_CODE = "MSCU66A9871";
    private static final String INVALID_CHECK_DIGIT_CODE = "IEWU664721A";
    private static final String INVALID_CHECK_DIGIT_NUMBER_CODE = "IEWU6647215";

    private static final String ZERO = "0";
    private static final String NEGATIVE = "-20";
    private static final String NEGATIVE_2 = "-2";
    private static final String NOT_NUMERIC = "A";
    private static final String STACKS_1 = "1";
    private static final String STACKS_2 = "2";
    private static final String STACKS_3 = "3";
    private static final String STACKS_5 = "5";
    private static final String WEIGHT_2 = "2";
    private static final String WEIGHT_5 = "5";
    private static final String WEIGHT_20 = "20";
    private static final String WEIGHT_30 = "30";
    private static final String WEIGHT_80 = "80";
    private static final String WEIGHT_100 = "100";
    private static final String HEIGHT_1 = "1";
    private static final String HEIGHT_2 = "2";
    private static final String HEIGHT_3 = "3";
    private static final String HEIGHT_5 = "5";

    private static final boolean USE_TEXT_VERSION = false;
    private static final int NO_PAUSE = 0;
    private static final int SHORT_PAUSE = 1500;
    private static final int LONG_PAUSE = 3000;

    private static final Pattern CARGO_EXCEPTION_PATTERN = Pattern.compile("CargoException:.+");

    private FrameFixture testFrame;
    private JFrame frameUnderTest;

    /**
     * Turn on automated check to verify all Swing component updates are done in the Swing Event
     * Dispatcher Thread (EDT). The EDT ensures that the application never loses responsiveness to
     * user gestures.
     */
    @BeforeClass
    public static void setUpOnce() {
        FailOnThreadViolationRepaintManager.install();
    }

    /**
     * Ensures that frame is launched through FEST to ensure the EDT is used properly. JUnit runs in
     * its own thread.
     */
    @Before
    public void setUp() {
        if (USE_TEXT_VERSION) {
            /* +----------------------------------------------+ */
            /* | Use a CargoTextFrame to test the application | */
            /* +----------------------------------------------+ */
            frameUnderTest = GuiActionRunner.execute(new GuiQuery<CargoTextFrame>() {
                @Override
                protected CargoTextFrame executeInEDT() {
                    CargoTextFrame cargo = new CargoTextFrame("Cargo Manifest 1.0");
                    return cargo;
                }
            });
        } else {
            /* +------------------------------------------+ */
            /* | Use a CargoFrame to test the application | */
            /* +------------------------------------------+ */
            frameUnderTest = GuiActionRunner.execute(new GuiQuery<CargoFrame>() {
                @Override
                protected CargoFrame executeInEDT() {
                    CargoFrame cargo = new CargoFrame("Cargo Manifest 1.0");
                    return cargo;
                }
            });
        }

        // Choose one of the above JFrames to use as the test fixture.
        testFrame = new FrameFixture(frameUnderTest);
    }

    /**
     * Unload the frame and associated resources.
     */
    @After
    public void tearDown() {
        delay(SHORT_PAUSE);
//        delay(NO_PAUSE);
        testFrame.cleanUp();
    }

    /**
     * Add a delay so that screen status can be viewed between tests.
     * Select from NO_PAUSE, SHORT_PAUSE, LONG_PAUSE.
     *
     * @param milliseconds The amount of time fow which to pause.
     */
    private void delay(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Ensure the frame used for testing has been instantiated.
     */
    @Test
    public void testConstruction() {
        assertNotNull(testFrame);
    }

    /**
     * Tests that only the "New Manifest" button is enabled when the application starts.
     */
    @Test
    public void buttonStateAtStart() {
        testFrame.button(NEW_MANIFEST).requireEnabled();
        testFrame.button(LOAD).requireDisabled();
        testFrame.button(UNLOAD).requireDisabled();
        testFrame.button(FIND).requireDisabled();
    }

    /*
     * Helper - Brings up a ManifestDilaog for further interaction in tests.
     */
    private DialogFixture prepareManifestDialog() {
        testFrame.button(NEW_MANIFEST).click();
        DialogFixture manifestFixture = testFrame.dialog(NEW_MANIFEST);
        return manifestFixture;
    }

    /*
     * Helper - Puts text in the relevant text areas of the ManifestDialog.
     */
    private void manifestDialogEnterText(DialogFixture dialog, String stacks, String height, String weight) {
        if (stacks != null) {
            dialog.textBox(NUMBER_OF_STACKS).enterText(stacks);
        }
        if (height != null) {
            dialog.textBox(MAXIMUM_HEIGHT).enterText(height);
        }
        if (weight != null) {
            dialog.textBox(MAXIMUM_WEIGHT).enterText(weight);
        }
        dialog.button("OK").click();
    }

    /**
     * Tests that only the "New Manifest" button is enabled when the "Cancel" Button pressed ManifestDialog.
     */
    @Test
    public void newManifestCancelCreateButtonCheck() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestFixture.button("Cancel").click();
        testFrame.button(NEW_MANIFEST).requireEnabled();
        testFrame.button(LOAD).requireDisabled();
        testFrame.button(UNLOAD).requireDisabled();
        testFrame.button(FIND).requireDisabled();
    }
    
    /**
     * Tests for an error message when no text is entered in a ManifestDialog and "OK" is pressed.
     */
    @Test
    public void newManifestNoData() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestFixture.button("OK").click();
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests that Zero Data Will be Accepted as a new Cargo Manifest
     */
    @Test
    public void newManifestZeroData() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, ZERO, ZERO, ZERO);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that Zero Stacks Will be Accepted as a new Cargo Manifest
     */
    @Test
    public void newManifestZeroStacks() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, ZERO, HEIGHT_5, WEIGHT_100);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that Zero Height Will be Accepted as a new Cargo Manifest
     */
    @Test
    public void newManifestZeroHeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, ZERO, WEIGHT_100);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("||  ||\n||  ||\n||  ||\n||  ||\n||  ||\n", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that Zero Weight Will be Accepted as a new Cargo Manifest
     */
    @Test
    public void newManifestZeroWeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, HEIGHT_5, ZERO);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("||  ||\n||  ||\n||  ||\n||  ||\n||  ||\n", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that Zero Stacks and Zero Height Will be Accepted as a new Cargo Manifest
     */
    @Test
    public void newManifestZeroStacksZeroHeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, ZERO, ZERO, WEIGHT_100);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that Zero Stacks and Zero Weight Will be Accepted as a new Cargo Manifest
     */
    @Test
    public void newManifestZeroStacksZeroWeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, ZERO, HEIGHT_5, ZERO);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that Zero Height and Zero Weight Will be Accepted as a new Cargo Manifest
     */
    @Test
    public void newManifestZeroHeightZeroWeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, ZERO, ZERO);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("||  ||\n||  ||\n||  ||\n||  ||\n||  ||\n", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests for an error message when non-numeric data is entered in all the text fields
     * in a ManifestDialog.
     */
    @Test
    public void newManifestInvalidData() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, NOT_NUMERIC, NOT_NUMERIC, NOT_NUMERIC);
        manifestFixture.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message when non-numeric data is entered in "Number of Stacks" text field
     * in a ManifestDialog.
     */
    @Test
    public void newManifestInvalidStacks() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, NOT_NUMERIC, null, null);
        manifestFixture.optionPane().requireErrorMessage();
    }

    /**
     * Tests for an error message when non-numeric data is entered in "Max Stack Height" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestValidStacksInvalidHeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, NOT_NUMERIC, null);
        manifestFixture.optionPane().requireErrorMessage();
    }

    /**
     * Tests for an error message when non-numeric data is entered in "Max Weight" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestValidStacksValidHeightInvalidWeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, HEIGHT_5, NOT_NUMERIC);
        manifestFixture.optionPane().requireErrorMessage();
    }

    /**
     * Tests for an error message when negative data is entered in "Number of Stacks" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestNegativeStacks() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, NEGATIVE, HEIGHT_5, WEIGHT_100);
        manifestFixture.optionPane().requireErrorMessage();
        manifestFixture.optionPane().requireMessage(CARGO_EXCEPTION_PATTERN);
    }

    /**
     * Tests for an error message when negative data is entered in "Max Stack Height" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestNegativeHeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, NEGATIVE, WEIGHT_100);
        manifestFixture.optionPane().requireErrorMessage();
        manifestFixture.optionPane().requireMessage(CARGO_EXCEPTION_PATTERN);
    }

    /**
     * Tests for an error message when negative data is entered in "Max Weight" text
     * field in a ManifestDialog.
     */
    @Test
    public void newManifestNegativeWeight() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, HEIGHT_5, NEGATIVE);
        manifestFixture.optionPane().requireErrorMessage();
        manifestFixture.optionPane().requireMessage(CARGO_EXCEPTION_PATTERN);
    }

    /**
     * Tests that all buttons are enabled on the window after a valid CargoManifest has been
     * created.
     */
    @Test
    public void newManifestValidDataButtonCheck() {
        DialogFixture manifestFixture = prepareManifestDialog();
        manifestDialogEnterText(manifestFixture, STACKS_5, HEIGHT_5, WEIGHT_100);
        testFrame.button(NEW_MANIFEST).requireEnabled();
        testFrame.button(LOAD).requireEnabled();
        testFrame.button(UNLOAD).requireEnabled();
        testFrame.button(FIND).requireEnabled();
    }

    /**
     * Tests that the Cargo Text Area holds the correct string representation for a single empty
     * stack.
     */
    @Test
    public void newManifestOneEmptyStack() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_100);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("||  ||\n", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that the Cargo Text Area holds the correct string representation for three empty
     * stacks.
     */
    @Test
    public void newManifestThreeEmptyStacks() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_100);
        if (frameUnderTest instanceof CargoTextFrame) {
            assertEquals("||  ||\n||  ||\n||  ||\n", testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /*
     * Helper - Enters data into a Create Manifest dialog
     */
    private void loadContainer(String containerType, String code, String weight,
            String goodsCat, String temperature) {
        testFrame.button(LOAD).click();
        DialogFixture containerDialog = testFrame.dialog(CONTAINER_INFORMATION);
        containerDialog.comboBox(CONTAINER_TYPE).selectItem(containerType);
        containerDialog.textBox(CONTAINER_CODE).enterText(code);
        containerDialog.textBox(CONTAINER_WEIGHT).enterText(weight);
        if (containerType.equals(DANGEROUS_GOODS)) {
            containerDialog.textBox(GOODS_CATEGORY).enterText(goodsCat);
        } else if (containerType.equals(REFRIGERATED_GOODS)) {
            containerDialog.textBox(TEMPERATURE2).enterText(temperature);
        }
        containerDialog.button("OK").click();
    }

    /**
     *  Tests for an error message shows when a the format of container code is invalid.
     */
    @Test
    public void ContainerCodeInvalidCodeLength() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, INVALID_CODE_LENGTH, null, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when a the format of container code is invalid.
     */
    @Test
    public void ContainerCodeInvalidIntegerOwnerCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, INVALID_OWNER_CODE_INTEGER, null, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when a the format of container code is invalid.
     */
    @Test
    public void ContainerCodeInvalidLowerCaseOwnerCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, INVALID_OWNER_CODE_LOWER_CASE, null, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when a the format of container code is invalid.
     */
    @Test
    public void ContainerCodeInvalidIntegerIdentifierCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, INVALID_IDENTIFIER_CODE_INTEGER, null, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when a the format of container code is invalid.
     */
    @Test
    public void ContainerCodeInvalidLowerCaseIdentifierCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, INVALID_IDENTIFIER_CODE_LOWER_CASE, null, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when a the format of container code is invalid.
     */
    @Test
    public void ContainerCodeInvalidSerialNumber() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, INVALID_SERIAL_NUMBER_CODE, null, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when a the format of container code is invalid.
     */
    @Test
    public void ContainerCodeInvalidCheckDigit() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, INVALID_CHECK_DIGIT_CODE, null, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when a the format of container code is invalid.
     */
    @Test
    public void ContainerCodeInvalidCheckDigitNumber() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, INVALID_CHECK_DIGIT_NUMBER_CODE, null, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests that a valid General Goods container is added and displayed.
     */
    @Test
    public void addGeneralGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that a valid Dangerous Goods container is added and displayed.
     */
    @Test
    public void addDangerousGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_3 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that a valid Refrigerated Goods container is added and displayed.
     */
    @Test
    public void addRefrigeratedGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_2 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that one valid container of each container type is added to the canvas.
     */
    @Test
    public void addOneOfEach() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                    + START_BARS + CODE_2 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that three valid container of each container type is added to the canvas.
     */
    @Test
    public void addThreeOfEach() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_3, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_5, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_5, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_5, CATEGORY_2, null);
        loadContainer(GENERAL_GOODS, CODE_4, WEIGHT_5, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_5, WEIGHT_5, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_6, WEIGHT_5, CATEGORY_2, null);
        loadContainer(GENERAL_GOODS, CODE_7, WEIGHT_5, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_8, WEIGHT_5, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_9, WEIGHT_5, CATEGORY_2, null);
        
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_4 + BETWEEN_BARS + CODE_7 + END_BARS + NEW_LINE
                    + START_BARS + CODE_2 + BETWEEN_BARS + CODE_5 + BETWEEN_BARS + CODE_8 + END_BARS + NEW_LINE
                    + START_BARS + CODE_3 + BETWEEN_BARS + CODE_6 + BETWEEN_BARS + CODE_9 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     *  Tests for an error message shows when there is no stack to load container
     */
    @Test
    public void ZeroStacksLoadContainer() {
        manifestDialogEnterText(prepareManifestDialog(), ZERO, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests to display the container while the height is zero
     */
    @Test
    public void ZeroHeightLoadContainer() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, ZERO, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     *  Tests for an error message shows when the container loaded into the zero weight manifest
     */
    @Test
    public void ZeroWeightLoadContainer() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, ZERO);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_5, null, null);
        testFrame.optionPane().requireErrorMessage();
    }

    /**
     *  Tests for an error message shows when the container loaded into the zero data manifest
     */
    @Test
    public void ZeroDataLoadContainer() {
        manifestDialogEnterText(prepareManifestDialog(), ZERO, ZERO, ZERO);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_5, null, null);
        testFrame.optionPane().requireErrorMessage();
    }

    /**
     *  Tests for an error message shows when the container loaded into the zero Stacks and Height manifest
     */
    @Test
    public void ZeroStacksZeroHeightLoadContainer() {
        manifestDialogEnterText(prepareManifestDialog(), ZERO, ZERO, WEIGHT_80);
        loadContainer(GENERAL_GOODS, CODE_9, WEIGHT_5, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when the container loaded into the zero Stacks and Height manifest
     */
    @Test
    public void ZeroStacksZeroWeightLoadContainer() {
        manifestDialogEnterText(prepareManifestDialog(), ZERO, HEIGHT_3, ZERO);
        loadContainer(GENERAL_GOODS, CODE_9, WEIGHT_5, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     *  Tests for an error message shows when the container loaded into the zero Stacks and Height manifest
     */
    @Test
    public void ZeroHeightZeroWeightLoadContainer() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, ZERO, ZERO);
        loadContainer(GENERAL_GOODS, CODE_9, WEIGHT_5, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message shows when the container have entered an invalid weight.
     */
    @Test
    public void ContainerInvalidWeight() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, NOT_NUMERIC, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message shows when the container have a weight less than 4 or more than 30.
     */
    @Test
    public void ContainerOverWeight() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_80, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message shows when the container have a weight less than 4 or more than 30.
     */
    @Test
    public void ContainerUnderWeight() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_2, null, null);
        testFrame.optionPane().requireErrorMessage();
    }

    /**
     * Tests for an error message shows when the category of <code>DangerousGoods</code> is not an Integer.
     */
    @Test
    public void DangerousGoodsInvalidCategory() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(DANGEROUS_GOODS, CODE_8, WEIGHT_30, NOT_NUMERIC, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message shows when the category of <code>DangerousGoods</code> is <code>null</code>.
     */
    @Test
    public void DangerousGoodsNullCategory() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(DANGEROUS_GOODS, CODE_8, WEIGHT_30, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message shows when the category of <code>DangerousGoods</code> is less than 1 or more than 9.
     */
    @Test
    public void DangerousGoodsLowerCategory() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(DANGEROUS_GOODS, CODE_8, WEIGHT_30, NEGATIVE_2, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message shows when the category of <code>DangerousGoods</code> is less than 1 or more than 9.
     */
    @Test
    public void DangerousGoodsHigherCategory() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(DANGEROUS_GOODS, CODE_7, WEIGHT_30, DANGER_HIGH_CATEGORY, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message shows when the category of <code>RefrigeratedGoods</code> is <code>null</code>.
     */
    @Test
    public void RefrigeratedGoodsNullTemp() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(REFRIGERATED_GOODS, CODE_4, WEIGHT_30, null, null);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests for an error message shows when the category of <code>RefrigeratedGoods</code> is not an integer.
     */
    @Test
    public void RefrigeratedGoodsInvalidTemp() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(REFRIGERATED_GOODS, CODE_4, WEIGHT_30, null, NOT_NUMERIC);
        testFrame.optionPane().requireErrorMessage();
    }

    /**
     * Tests that a valid Refrigerated Goods container is added and displayed.
     */
    @Test
    public void RefrigeratedGoodsHighTemp() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(REFRIGERATED_GOODS, CODE_6, WEIGHT_20, null, TEMPERATURE_HIGH_100);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_6 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
  /**
    * Tests that a valid General Goods and Dangerous Goods container is added and displayed.
    */
   @Test
   public void addGeneralGoodsAndDangerousGoods() {
       manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_2, WEIGHT_80);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
       loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_3 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }

   /**
    * Tests that a valid General Goods and Refrigerated Goods container is added and displayed.
    */
   @Test
   public void addGeneralGoodsAndRefrigeratedGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_2, WEIGHT_80);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
       loadContainer(REFRIGERATED_GOODS, CODE_5, WEIGHT_30, null, TEMPERATURE_MINUS_4);
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_5 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
   
   /**
    * Tests that a valid Dangerous Goods and Refrigerated Goods container is added and displayed.
    */
   @Test
   public void addDangerousGoodsAndRefrigeratedGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_2, WEIGHT_80);
       loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
       loadContainer(REFRIGERATED_GOODS, CODE_5, WEIGHT_30, null, TEMPERATURE_MINUS_4);
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_3 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_5 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }

   /**
    * Tests that two valid General Goods container is added and displayed in the same stack.
    */
   @Test
   public void OneStackTwoHeightLoadTwoGeneralGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_2, WEIGHT_80);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
       loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_20, null, null);
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_5 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
   
   /**
    * Tests for adding two different container loaded into same stack.
    */
   @Test
   public void OneStackTwoHeightLoadTwoDifferentGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_2, WEIGHT_80);
       loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
       loadContainer(REFRIGERATED_GOODS, CODE_5, WEIGHT_30, null, TEMPERATURE_MINUS_4);
       testFrame.optionPane().requireErrorMessage();
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_3 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
  
   /**
    * Tests for adding four dangerous container loaded into a stack with height of 3.
    */
   @Test
   public void OneStackThreeHeightLoadFourDangerousGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_3, WEIGHT_80);
       loadContainer(DANGEROUS_GOODS, CODE_2, WEIGHT_20, CATEGORY_2, null);
       loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_20, CATEGORY_2, null);
       loadContainer(DANGEROUS_GOODS, CODE_6, WEIGHT_20, CATEGORY_2, null);
       loadContainer(DANGEROUS_GOODS, CODE_8, WEIGHT_20, CATEGORY_2, null);
       testFrame.optionPane().requireErrorMessage();
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_2 + BETWEEN_BARS + CODE_4 + BETWEEN_BARS + CODE_6 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
   
   /**
    * Tests for two dangerous container loaded into two stacks with height of 1.
    */
   @Test
   public void TwoStacksOneHeightLoadTwoDangerousGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_1, WEIGHT_80);
       loadContainer(DANGEROUS_GOODS, CODE_2, WEIGHT_20, CATEGORY_2, null);
       loadContainer(DANGEROUS_GOODS, CODE_4, WEIGHT_20, CATEGORY_2, null);
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_2 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_4 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
   
   /**
    * Tests for two different types of container loaded into two stacks with height of 1.
    */
   @Test
   public void TwoStacksOneHeightLoadTwoDifferentGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_1, WEIGHT_80);
       loadContainer(DANGEROUS_GOODS, CODE_2, WEIGHT_20, CATEGORY_2, null);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_2 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_1 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
  
   /**
    * Tests for adding three types of container loaded into two stack with height of 1.
    */
   @Test
   public void TwoStacksOneHeightLoadThreeDifferentGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_1, WEIGHT_80);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
       loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
       loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
       testFrame.optionPane().requireErrorMessage();
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_2 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
   
   /**
    * Tests for adding three types of container loaded into two stack with height of 5.
    */
   @Test
   public void TwoStacksFiveHeightLoadThreeDifferentGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_2, HEIGHT_5, WEIGHT_80);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
       loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
       loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
       testFrame.optionPane().requireErrorMessage();
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_2 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
    
   /**
    * Tests for adding three types of container loaded into three stacks with height of 5.
    */
   @Test
   public void ThreeStacksLoadThreeDifferentGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_5, WEIGHT_80);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
       loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
       loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_2 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_3 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
   
   /**
    * Tests for adding two refrigerated container loaded into three stacks with height of 1.
    */
   @Test
   public void ThreeStacksOneHeightLoadTwoRefrigeratedGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_80);
       loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
       loadContainer(REFRIGERATED_GOODS, CODE_5, WEIGHT_20, null, TEMPERATURE_MINUS_4);
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_2 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_5 + END_BARS + NEW_LINE
        		   + START_BARS + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
   
   /**
    * Tests for adding four dangerous container loaded into three stacks with height of 1.
    */
   @Test
   public void ThreeStackOneHeightLoadFourDangerousGoods() {
	   manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_100);
       loadContainer(DANGEROUS_GOODS, CODE_1, WEIGHT_30, CATEGORY_2, null);
       loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
       loadContainer(DANGEROUS_GOODS, CODE_5, WEIGHT_30, CATEGORY_2, null);
       loadContainer(DANGEROUS_GOODS, CODE_7, WEIGHT_30, CATEGORY_2, null);
       testFrame.optionPane().requireErrorMessage();
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_3 + END_BARS + NEW_LINE
        		   + START_BARS + CODE_5 + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
    
   /**
    * Tests that adding three 30 tonne containers overloads a manifest of 20 tonnes.
    */
   @Test
   public void OneGoodOverload() {
       manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_20);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
       testFrame.optionPane().requireErrorMessage();
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + END_BARS + NEW_LINE
        		   + START_BARS + END_BARS + NEW_LINE
        		   + START_BARS + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }
   
    /**
     * Tests that adding three 30 tonne containers overloads a manifest of 80 tonnes.
     */
   @Test
   public void overLoad() {
       manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_80);
       loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_30, null, null);
       loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_30, null, TEMPERATURE_MINUS_4);
       loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_30, CATEGORY_2, null);
       testFrame.optionPane().requireErrorMessage();
       if (frameUnderTest instanceof CargoTextFrame) {
           String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
                   + START_BARS + CODE_2 + END_BARS + NEW_LINE
                   + START_BARS + END_BARS + NEW_LINE;
           assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
       }
   }

    /*
     * Helper - Clicks the Unload button and enters a valid container code.
     */
    private void unloadContainer(String code) {
        testFrame.button(UNLOAD).click();
        DialogFixture containerDialog = testFrame.dialog("Container Dialog");
        containerDialog.textBox(CONTAINER_CODE).enterText(code);
        containerDialog.button("OK").click();
    }

    /**
     * Tests that a unload a container without load.
     */
    @Test
    public void UnloadWithoutLoad() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests that a loaded container can then be unloaded.
     */
    @Test
    public void loadGeneralGoodsThenUnload() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that a unload a container with an invalid code.
     */
    @Test
    public void UnloadWithInvalidCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(INVALID_CODE_LENGTH);
        testFrame.optionPane().requireErrorMessage();
    }
    
    /**
     * Tests that a loaded container can then be unloaded.
     */
    @Test
    public void LoadRefrigeratedGoodsThenUnloadDifferentGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_2 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        testFrame.optionPane().requireErrorMessage();
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_2 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that a loaded 3 container in the same stack then unloaded the bottom container.
     */
    @Test
    public void loadThreeGeneralGoodsInOneStackUnloadFirstGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_5, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_9, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_5 + BETWEEN_BARS + CODE_9 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        testFrame.optionPane().requireErrorMessage();
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_5 + BETWEEN_BARS + CODE_9 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that a loaded 3 container in the same stack then unloaded the top container.
     */
    @Test
    public void loadThreeGeneralGoodsInOneStackUnloadThirdGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_5, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_9, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_5 + BETWEEN_BARS + CODE_9 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_9);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_5 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that a loaded 3 container in the same stack then unloaded from top container.
     */
    @Test
    public void loadThreeGeneralGoodsInOneStackUnloadFromThreeToOne() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_5, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_9, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_5 + BETWEEN_BARS + CODE_9 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_9);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_5 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_5);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 +END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that a loaded 3 different container in the different stack then unloaded the second container.
     */
    @Test
    public void loadOneOfEachGoodsUnloadSecondGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_5, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(REFRIGERATED_GOODS, CODE_2, WEIGHT_20, null, TEMPERATURE_MINUS_4);
        loadContainer(DANGEROUS_GOODS, CODE_3, WEIGHT_20, CATEGORY_2, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_2 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_2);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that a loaded 3 general container in different stacks then unloaded the first container.
     */
    @Test
    public void loadThreeGeneralGoodsInThreeStackUnloadFirstGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_9, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_5 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_9 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        unloadContainer(CODE_1);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE
            		+ START_BARS + CODE_5 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_9 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /*
     * Helper - Clicks the Find button and enters a valid container code.
     */
    private void findContainer(String code) {
        testFrame.button(FIND).click();
        DialogFixture containerDialog = testFrame.dialog("Container Dialog");
        containerDialog.textBox(CONTAINER_CODE).enterText(code);
        containerDialog.button("OK").click();
    }

    /**
     * Tests that find a container that have been loaded.
     */
    @Test
    public void loadGeneralGoodsThenFind() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        findContainer(CODE_1);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_FOUND_BARS + CODE_1 + END_FOUND_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that find a container that have been loaded.
     */
    @Test
    public void loadThreeGeneralGoodsThenFind() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_5, WEIGHT_80);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + BETWEEN_BARS + CODE_3 + BETWEEN_BARS + CODE_5 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        findContainer(CODE_3);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + START_FOUND_BETWEEN_BARS + CODE_3 + END_FOUND_BETWEEN_BARS + CODE_5 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that find a container that have been loaded.
     */
    @Test
    public void loadThreeGeneralGoodsThenFindDiffStacks() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_3, HEIGHT_1, WEIGHT_100);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_3, WEIGHT_20, null, null);
        loadContainer(GENERAL_GOODS, CODE_5, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_5 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        findContainer(CODE_1);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_FOUND_BARS + CODE_1 + END_FOUND_BARS + NEW_LINE
            		+ START_BARS + CODE_3 + END_BARS + NEW_LINE
            		+ START_BARS + CODE_5 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
    
    /**
     * Tests that find a container which have not been loaded.
     */
    @Test
    public void FindWithoutLoadGoods() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        findContainer(CODE_1);
        testFrame.optionPane().requireErrorMessage();
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }

    /**
     * Tests that find a container which have not been loaded.
     */
    @Test
    public void FindWithDifferentCode() {
        manifestDialogEnterText(prepareManifestDialog(), STACKS_1, HEIGHT_1, WEIGHT_30);
        loadContainer(GENERAL_GOODS, CODE_1, WEIGHT_20, null, null);
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
        findContainer(CODE_3);
        testFrame.optionPane().requireErrorMessage();
        if (frameUnderTest instanceof CargoTextFrame) {
            String expected = START_BARS + CODE_1 + END_BARS + NEW_LINE;
            assertEquals(expected, testFrame.textBox(CARGO_TEXT_AREA).text());
        }
    }
}