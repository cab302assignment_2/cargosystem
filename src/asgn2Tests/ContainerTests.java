package asgn2Tests;

/* Some valid container codes used in the tests below:
 * INKU2633836
 * KOCU8090115
 * MSCU6639871
 * CSQU3054389
 */

import static org.junit.Assert.*;

import org.junit.*;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.InvalidContainerException;

/**
 * Test for <code>ContainerCode, FreightContainer, GeneralGoodsContainer, DangerousGoodsContainer, RefrigeratedContainer</code> class.
 * 
 * @author Rohan Treadwell n9184856
 * @version 1.0
 **/
public class ContainerTests {
	//Implementation Here - includes tests for ContainerCode and for the actual container classes. 
	//private FreightContainer FC;
	private ContainerCode CC;
	private ContainerCode CC1;
	private ContainerCode CC2;
	
	private GeneralGoodsContainer GGC;
	private DangerousGoodsContainer Danger;
	private RefrigeratedContainer RC;
	

	private int NegativecontainerWeight = -3;
	private int LowcontainerWeight = 3;
	private int containerWeight = 15;
	private int HighcontainerWeight = 40;

	private int NegativeDangerClass = -1;
	private int lowDangerClass = 0;
	private int dangerClass = 4;
	private int highDangerClass = 10;

	private int NegativefridgeTemp = -5;
	private int fridgeTemp = 5;
	private int highFridgeTemp = 100;
	/*
	 * this class it to test as below:
	 * 
	 * 1) Freight Container
	 * 2) GeneralGoodsContainer
	 * 3) RefrigeratedContainer
	 * 4) DangerousGoodsContainer
	 * 5) Container Code
	 * 
	 * In the package asgn2Containers
	 * 
	 */
	
	@Before
	public void setUp() throws Exception{
		CC2 = new ContainerCode("FNPU6952577");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of container code is <code>incorrect</code>
	 */
	@Test
	public void ContainerCode() throws InvalidCodeException{
		CC = new ContainerCode("FNPU6952577");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of container code is not <code>11 character long</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeInvalidCodeLength() throws InvalidCodeException{
		CC = new ContainerCode("FNPU677");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the container code is <code>null</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeNullCode() throws InvalidCodeException{
		CC = new ContainerCode(null);
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of owner code is not <code>Upper Case letter</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeInvalidOwnerCodeLowerCase() throws InvalidCodeException{
		CC = new ContainerCode("fnPU6952577");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of owner code is not <code>Upper Case letter</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeInvalidOwnerCodeDigits() throws InvalidCodeException{
		CC = new ContainerCode("12PU6952577");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of owner code is not <code>Upper Case letter</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeInvalidCategoryCode() throws InvalidCodeException{
		CC = new ContainerCode("FNPu6952577");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of owner code is not <code>Upper Case letter</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeInvalidCategoryCodeDigits() throws InvalidCodeException{
		CC = new ContainerCode("FNP06952577");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of Serial code is not <code>6 digit number</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeInvalidSerialCode() throws InvalidCodeException{
		CC = new ContainerCode("FNPU6AD2577");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of Check Digit is <code>not digit number</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeInvalidCheckDigit() throws InvalidCodeException{
		CC = new ContainerCode("FNPU695257H");
	}
	
	/**
	 * Test case for <code>ContainerCode</code> class.
	 * Before load the container each container will be assigned with an 11 character long unique code.
	 * The code will have its format which are ownerCode, CategoryCode, SerialCode and also a Check Digit
	 * OwnerCode - the first 3 upper Case letter in the code
	 * Category Code - the 4th Letter in the code must be a 'U'
	 * Serial code - 6 digit after the category code
	 * Check Digit - is the remaining of the sum of all digits and alphabets divide by 10  
	 *
	 *
	 * @throws InvalidCodeException if the format of Check Digit is <code>incorrect</code>
	 */
	@Test (expected = InvalidCodeException.class) 
	public void ContainerCodeIncorrectModDigit() throws InvalidCodeException{
		CC = new ContainerCode("FNPU6952573");
	}
	
	/**
	 * Test case for <code>toString</code> class.
	 *is a function to return the container code
	 *
	 *
	 * @throws InvalidCodeException
	 */
	@Test
	public void CCtoString() throws InvalidCodeException{
		CC = new ContainerCode("FNPU6952577");
		assertEquals("FNPU6952577",CC.toString());
	}
	
	/**
	 * Test case for <code>equals</code> class.
	 * It will return true if the code of the container are same, else will return false
	 *
	 *
	 * @throws InvalidCodeException
	 */
	@Test
	public void equalTest() throws InvalidCodeException{
		CC = new ContainerCode("FNPU6952577");
		assertTrue(CC.equals(CC));
	}
	
	/**
	 * Test case for <code>equals</code> class.
	 * It will return true if the code of the container are same, else will return false
	 *
	 *
	 * @throws InvalidCodeException
	 */
	@Test
	public void DiffequalTest() throws InvalidCodeException{
		CC = new ContainerCode("FNPU6952577");
		CC1 = new ContainerCode("ASHU3506054");
		assertFalse(CC1.equals(CC));
	}
	
	/**
	 * Test Case for <code>FreightContainer</code> class.
	 * Due to Freight container is an abstract class, so have to test through other Container class.
	 * 
	 * 
	 * @throws InvalidContainerException
	 */
	@Test
	public void TestContainer() throws InvalidContainerException{
		GGC = new GeneralGoodsContainer(CC2,containerWeight);
	}	
	 
	 /**
	  * Test Case for <code>FreightContainer</code> class.
	  * Due to Freight container is an abstract class, so have to test through other Container class.
	  * 
	  * 
	  * @throws InvalidContainerException If the container code is <code>null</code>
	  */
	 @Test (expected = InvalidContainerException.class)
	 public void TestContainerNullCode() throws InvalidContainerException{
		GGC = new GeneralGoodsContainer(null,containerWeight);
	 }
	 
	 /**
	  * Test Case for <code>FreightContainer</code> class.
	  * Due to Freight container is an abstract class, so have to test through other Container class.
	  * 
	  * 
	  * @throws InvalidContainerException if the <code>grossWeight</code> is lower than 4 or more than 30
	  */
	 @Test (expected = InvalidContainerException.class)
	 public void TestContainerUnderWeightCode() throws InvalidContainerException{
		 GGC = new GeneralGoodsContainer(CC2,LowcontainerWeight);
	 }	 
	 
	 /**
	  * Test Case for <code>FreightContainer</code> class.
	  * Due to Freight container is an abstract class, so have to test through other Container class.
	  * 
	  * 
	  * @throws InvalidContainerException if the <code>grossWeight</code> is lower than 4 or more than 30
	  */
	 @Test (expected = InvalidContainerException.class)
	 public void TestContainerNegativeWeightCode() throws InvalidContainerException{
		 GGC = new GeneralGoodsContainer(CC2,NegativecontainerWeight);
	 }
	 
	 /**
	  * Test Case for <code>FreightContainer</code> class.
	  * Due to Freight container is an abstract class, so have to test through other Container class.
	  * 
	  * 
	  * @throws InvalidContainerException if the <code>grossWeight</code> is lower than 4 or more than 30
	  */
	 @Test (expected = InvalidContainerException.class)
	 public void TestContainerOverWeightCode() throws InvalidContainerException{
			GGC = new GeneralGoodsContainer(CC2,HighcontainerWeight);
	 }	 
	 
	 /**
	  * Test Case for <code>FreightContainer</code> class.
	  * Due to Freight container is an abstract class, so have to test through other Container class.
	  * 
	  * 
	  * @throws InvalidContainerException if the <code>grossWeight</code> is <code>null</code>
	  */
	 @Test (expected = InvalidContainerException.class)
	 public void TestContainerNullWeightCode() throws InvalidContainerException{
			GGC = new GeneralGoodsContainer(CC2,null);
	 }	 
	 
	/**
	 * Test Case for <code>FreightContainer</code> class.
	 * Due to Freight container is an abstract class, so have to test through other Container class.
	 * 
	 * 
	 * @throws InvalidContainerException
	 */
	@Test
	public void TestContainerGetCode() throws InvalidContainerException{
		GGC = new GeneralGoodsContainer(CC2,containerWeight);
		assertEquals(CC2,GGC.getCode());
	}

	/**
	 * Test Case for <code>FreightContainer</code> class.
	 * Due to Freight container is an abstract class, so have to test through other Container class.
	 * 
	 * 
	 * @throws InvalidContainerException
	 */
	@Test
	public void TestContainerGetWeight() throws InvalidContainerException{
		GGC = new GeneralGoodsContainer(CC2,containerWeight);
		Integer i = containerWeight;
		assertEquals(i,GGC.getGrossWeight());
	}
	
	/**
	 * Test Case for <code>DangerousGoodsContainer</code> class.
	 * DangerousGoodsContainer required to enter the category of goods in a range of 1 ~ 9
	 * 
	 * 
	 * @throws InvalidContainerException if the <code>Category</code> is <code>null</code>
	 */
	@Test (expected = InvalidContainerException.class)
	public void TestDangerContainerNullCat() throws InvalidContainerException{
		Danger = new DangerousGoodsContainer(CC2,containerWeight,null);
	}
	
	/**
	 * Test Case for <code>DangerousGoodsContainer</code> class.
	 * DangerousGoodsContainer required to enter the category of goods in a range of 1 ~ 9
	 * 
	 * 
	 * @throws InvalidContainerException if the <code>Category</code> is not in the range between 1-9
	 */
	@Test (expected = InvalidContainerException.class)
	public void TestDangerContainerLowCat() throws InvalidContainerException{
		Danger = new DangerousGoodsContainer(CC2,containerWeight,lowDangerClass);
	}
	
	/**
	 * Test Case for <code>DangerousGoodsContainer</code> class.
	 * DangerousGoodsContainer required to enter the category of goods in a range of 1 ~ 9
	 * 
	 * 
	 * @throws InvalidContainerException if the <code>Category</code> is not in the range between 1-9
	 */
	@Test (expected = InvalidContainerException.class)
	public void TestDangerContainerHighCat() throws InvalidContainerException{
		Danger = new DangerousGoodsContainer(CC2,containerWeight,highDangerClass);
	}
	
	/**
	 * Test Case for <code>DangerousGoodsContainer</code> class.
	 * DangerousGoodsContainer required to enter the category of goods in a range of 1 ~ 9
	 * 
	 * 
	 * @throws InvalidContainerException if the <code>Category</code> is not in the range between 1-9
	 */
	@Test (expected = InvalidContainerException.class)
	public void TestDangerContainerNegCat() throws InvalidContainerException{
		Danger = new DangerousGoodsContainer(CC2,containerWeight,NegativeDangerClass);
	}

	/**
	 * Test Case for <code>DangerousGoodsContainer</code> class.
	 * DangerousGoodsContainer required to enter the category of goods in a range of 1 ~ 9
	 * 
	 * 
	 * @throws InvalidContainerException if the <code>Category</code> is not in the range between 1-9
	 */
	@Test
	public void TestDangerContainerGetCategory() throws InvalidContainerException{
		Danger = new DangerousGoodsContainer(CC2,containerWeight,dangerClass);
		Integer i = dangerClass;
		assertEquals(i,Danger.getCategory());
	}
	
	/**
	 * Test Case for <code>RefrigeratedContainer</code> class.
	 * RefrigeratedContainer required to enter the temperature of goods in the container
	 * 
	 * 
	 * @throws InvalidContainerException if the Temperature is <code>null</code>
	 */
	@Test
	public void TestRefrigeratedContainer() throws InvalidContainerException{
		RC = new RefrigeratedContainer(CC2,containerWeight,fridgeTemp);
	}
	
	/**
	 * Test Case for <code>RefrigeratedContainer</code> class.
	 * RefrigeratedContainer required to enter the temperature of goods in the container
	 * 
	 * 
	 * @throws InvalidContainerException if the Temperature is <code>null</code>
	 */
	@Test (expected = InvalidContainerException.class)
	public void TestRefrigeratedContainerNullTemp() throws InvalidContainerException{
		RC = new RefrigeratedContainer(CC2,containerWeight,null);
	}
	
	/**
	 * Test Case for <code>RefrigeratedContainer</code> class.
	 * RefrigeratedContainer required to enter the temperature of goods in the container
	 * 
	 * 
	 * @throws InvalidContainerException if the Temperature is <code>null</code>
	 */
	@Test
	public void TestRefrigeratedContainerNegativeTemp() throws InvalidContainerException{
		RC = new RefrigeratedContainer(CC2,containerWeight,NegativefridgeTemp);
	}
	
	/**
	 * Test Case for <code>RefrigeratedContainer</code> class.
	 * RefrigeratedContainer required to enter the temperature of goods in the container
	 * 
	 * 
	 * @throws InvalidContainerException if the Temperature is <code>null</code>
	 */
	@Test
	public void TestRefrigeratedContainerHighTemp() throws InvalidContainerException{
		RC = new RefrigeratedContainer(CC2,containerWeight,highFridgeTemp);
	}
	
	/**
	 * Test Case for <code>getTemperature</code> class.
	 * temperature of RefrigeratedContainer will need to be track as well
	 * 
	 * 
	 * @throws InvalidContainerException
	 */
	@Test
	public void TestRefrigeratedContainerGetTemp() throws InvalidContainerException{
		RC = new RefrigeratedContainer(CC2,containerWeight,fridgeTemp);
		Integer i = fridgeTemp;
		assertEquals(i,RC.getTemperature());
	}
}
